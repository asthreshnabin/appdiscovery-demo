package com.foundation.demoproject.shared.base

import android.widget.Toast
import com.foundation.demoproject.R
import com.foundation.demoproject.shared.helper.ProgressDialog
import com.foundation.demoproject.shared.helper.ProgressDialogHelper
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView

/**
 * Created by asthreshnabin on 11/06/2021.
 */
abstract class BaseActivity<V : MvpView, P : MvpPresenter<V>> : MvpActivity<V, P>(), BaseView {
    private var progressDialogHelper: ProgressDialogHelper? = null

    override fun navigateToHomeActivity() {
        //Router.navigateActivity(this as AppCompatActivity, true, SearchActivity::class)
    }


    override fun showToast(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showToast(messageInt: Int) {
        showToast(getString(messageInt))
    }


    override fun hideLoading() {
        ProgressDialog.getInstance(this)?.dismissDialog()
    }

    override fun showLoading() {
        ProgressDialog.getInstance(this)?.showProgress(getString(R.string.loading))
    }

    override fun showLoading(message: Int) {

        ProgressDialog.getInstance(this)?.showProgress(getString(message))
    }

    override fun showLoading(message: String) {
        ProgressDialog.getInstance(this)?.showProgress(message)
    }

}