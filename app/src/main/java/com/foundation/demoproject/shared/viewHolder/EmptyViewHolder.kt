package com.foundation.demoproject.shared.viewHolder

import android.view.View
import com.foundation.demoproject.shared.base.BaseViewHolder

/**
 * @Created by: asthreshNabin
 * @Date: 19/11/2022
 */
class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView)