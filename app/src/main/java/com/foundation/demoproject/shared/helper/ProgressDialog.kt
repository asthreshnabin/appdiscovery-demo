package com.foundation.demoproject.shared.helper

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.foundation.demoproject.R

class ProgressDialog private constructor(context: Context) {
    private val context: Context?
    private var textView: TextView? = null

    fun showProgress(message: String?) {
        if ((context as Activity?)?.isFinishing == true)
            return

        if (context?.isFinishing != true) {
            try {
                setMessage(message)
                dialog?.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setMessage(message: String?) {
        if (message.isNullOrEmpty()) {
            textView?.visibility = View.INVISIBLE
            return
        }
        textView?.text = message
        textView?.visibility = View.VISIBLE
    }

    private val isShowing: Boolean
        get() = if (dialog != null) dialog!!.isShowing else false

    fun dismissDialog() {
        if (dialog != null && isShowing) {
            try {
                dialog?.dismiss()
            } catch (e: IllegalArgumentException) {
                dialog = null
                sInstance = null
            } catch (e: Exception) {
                e.printStackTrace()
            }
            dialog = null
            sInstance = null
        }
    }

    companion object {
        private var dialog: Dialog? = null
        private var sInstance: ProgressDialog? = null
        fun getInstance(context: Context): ProgressDialog? {
            return if (sInstance == null) ProgressDialog(context) else sInstance
        }
    }

    init {
        this.context = context
        if (dialog == null) {
            dialog = Dialog(this.context, R.style.newDialog)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.setContentView(R.layout.layout_progress_dialog)
            if (dialog?.window != null) dialog?.window!!.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(this.context, android.R.color.transparent)
                )
            )
            dialog?.setCancelable(false)
            textView = dialog?.findViewById<TextView>(R.id.txvMessage)
        }
    }
}