package com.foundation.demoproject.shared.base

import android.widget.Toast
import com.foundation.demoproject.R
import com.foundation.demoproject.shared.helper.ProgressDialog
import com.foundation.demoproject.shared.helper.ProgressDialogHelper
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView

/**
 * Created by asthreshnabin on 11/19/2022.
 */
abstract class BaseFragment<V : MvpView, P : MvpPresenter<V>> : MvpFragment<V, P>(), BaseView {

    private var progressDialogHelper: ProgressDialogHelper? = null

    override fun navigateToHomeActivity() {
        //Router.navigateActivity(context as AppCompatActivity, true, SearchActivity::class)
    }


    override fun showToast(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showToast(messageInt: Int) {
        showToast(getString(messageInt))
    }

    override fun hideLoading() {
        ProgressDialog.getInstance(requireContext())?.dismissDialog()
    }

    override fun showLoading() {
        ProgressDialog.getInstance(requireContext())?.showProgress(getString(R.string.loading))
    }

    override fun showLoading(message: Int) {

        ProgressDialog.getInstance(requireContext())?.showProgress(getString(message))
    }

    override fun showLoading(message: String) {
        ProgressDialog.getInstance(requireContext())?.showProgress(message)
    }


}