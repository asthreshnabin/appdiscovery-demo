package com.foundation.demoproject.shared.base

import androidx.test.espresso.idling.CountingIdlingResource

/**
 * Created by asthreshnabin on 11/19/2022.
 */
object BaseIdlingResource {
    private val countingIdlingResource = CountingIdlingResource("globalCountdown")

    fun increment() {
        countingIdlingResource.increment()
    }

    fun decrement() {
        if (!getIdlingResource().isIdleNow)
            countingIdlingResource.decrement()
    }

    fun getIdlingResource() = countingIdlingResource
}