package com.foundation.demoproject.shared.base

import com.hannesdorfmann.mosby3.mvp.MvpView

/**
 * Created by asthreshnabin on 11/19/2022.
 */
interface BaseView : MvpView {
    fun showToast(message: String?)

    fun showToast(messageInt: Int)

    fun navigateToHomeActivity()
//    fun getAppDatabase(): AppDatabase?

    fun hideLoading()

    fun showLoading(message: String)

    fun showLoading( message: Int)

    fun showLoading()

}
