package com.foundation.demoproject.shared.viewHolder

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.foundation.demoproject.shared.base.BaseViewHolder
import com.foundation.demoproject.utils.extensions.load
import com.usermodule.shared.model.response.MovieReview
import kotlinx.android.synthetic.main.review_item.view.*

/**
 * @Created by: asthreshNabin
 * @Date: 20/11/2022
 */
class MovieReviewViewHolder(
    itemView: View,
    private var context: Context?,
) : BaseViewHolder(itemView) {
    private val txvTitle: TextView? = itemView.txvMovieName
    private val imvMovie: ImageView? = itemView.imgMovieImage
    private val txvDescription: TextView? = itemView.txvOverView
    fun bind(movieData: MovieReview?) {
        txvTitle?.text = movieData?.author
        txvDescription?.text = movieData?.content
        imvMovie?.load(movieData?.authorDetails?.avatar_path?.substring(1))
    }


}