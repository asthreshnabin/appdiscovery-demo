package com.foundation.demoproject.shared.viewHolder

import android.content.Context
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.foundation.demoproject.BuildConfig
import com.foundation.demoproject.shared.base.BaseViewHolder
import com.foundation.demoproject.utils.extensions.OnViewClickListener
import com.foundation.demoproject.utils.extensions.load
import com.foundation.demoproject.utils.extensions.onClickListener
import com.usermodule.shared.model.response.MovieResponse
import kotlinx.android.synthetic.main.movie_item.view.*

/**
 * @Created by: asthreshNabin
 * @Date: 19/11/2022
 */
class MovieListViewHolder(
    itemView: View,
    private var context: Context?,
    private val onMovieItemClicked: (position: Int) -> Unit?,
) : BaseViewHolder(itemView) {
    private val txvTitle: TextView? = itemView.txvMovieName
    private val imvMovie: ImageView? = itemView.imgMovieImage
    private val txvDescription: TextView? = itemView.txvOverView
    private val txvReleaseDate: TextView? = itemView.txvReleaseDate
    private val txvAvgRating: TextView? = itemView.txvRating
    private val rllMovieItem: RelativeLayout? = itemView.rllItemView
    fun bind(movieData: MovieResponse?, position: Int) {
        txvTitle?.text = movieData?.title
        txvDescription?.text = movieData?.overview
        txvReleaseDate?.text = movieData?.releaseDate
        imvMovie.load("${BuildConfig.baseImageUrl}${movieData?.posterPath}")
        val s = SpannableString(
            """
              ${movieData?.averageVote}/10 
              """.trimIndent()
        )
        s.setSpan(RelativeSizeSpan(1.7f), 0, 3, 0)
        txvAvgRating?.text = s
        onClickListener(context = context, rllMovieItem, object : OnViewClickListener {
            override fun onClicked(context: Context?) {
                onMovieItemClicked(position)
            }
        })
    }


}