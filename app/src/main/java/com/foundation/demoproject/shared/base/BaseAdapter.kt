package com.foundation.demoproject.shared.base

import androidx.recyclerview.widget.RecyclerView
import io.reactivex.disposables.CompositeDisposable

/**
 * @Created by: asthreshNabin
 * @Date: 19/11/2022
 */
abstract class BaseAdapter<V : BaseViewHolder> : RecyclerView.Adapter<V>() {
    protected var compositeDisposable: CompositeDisposable? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        compositeDisposable = CompositeDisposable()
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        compositeDisposable?.dispose()
        super.onDetachedFromRecyclerView(recyclerView)
    }
}