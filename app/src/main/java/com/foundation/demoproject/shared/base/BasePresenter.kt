package com.foundation.demoproject.shared.base

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by asthreshnabin on 11/19/2022.
 */
abstract class BasePresenter<V : BaseView> : MvpBasePresenter<V>() {
    protected var compositeDisposable: CompositeDisposable? = null

    override fun attachView(view: V) {
        super.attachView(view)
        compositeDisposable = CompositeDisposable()
    }

    override fun detachView() {
        compositeDisposable?.dispose()
        super.detachView()
    }

}