package com.foundation.demoproject.shared.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * @Created by: asthreshNabin
 * @Date: 19/11/2022
 */
abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)