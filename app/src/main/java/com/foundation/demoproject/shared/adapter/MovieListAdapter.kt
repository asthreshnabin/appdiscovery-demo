package com.foundation.demoproject.shared.adapter

import android.content.Context
import android.view.ViewGroup
import com.foundation.demoproject.R
import com.foundation.demoproject.shared.base.BaseAdapter
import com.foundation.demoproject.shared.base.BaseViewHolder
import com.foundation.demoproject.shared.viewHolder.EmptyViewHolder
import com.foundation.demoproject.shared.viewHolder.MovieListViewHolder
import com.foundation.demoproject.utils.extensions.inflate
import com.usermodule.shared.model.response.MovieResponse
import com.usermodule.utils.constants.AdapterConstants
import java.util.*

/**
 * @Created by: asthreshNabin
 * @Date: 19/11/2022
 */
class MovieListAdapter(
    private var context: Context?,
    private val movieList: MutableList<MovieResponse?>?,
    private val onMovieItemClicked: (position: Int) -> Unit?,
) : BaseAdapter<BaseViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        AdapterConstants.sectionMovie -> MovieListViewHolder(
            parent.inflate(R.layout.movie_item), context
        ) { position ->
            onMovieItemClicked(position)
        }
        else -> EmptyViewHolder(parent.inflate(R.layout.adapter_empty))
    }

    override fun onBindViewHolder(baseViewHolder: BaseViewHolder, position: Int) {
        when (getItemViewType(position)) {
            AdapterConstants.sectionMovie -> populateFaces(baseViewHolder, position)
        }
    }

    override fun getItemCount() = movieList?.count() ?: 0

    override fun getItemViewType(position: Int) = when (movieList?.getOrNull(position)?.type) {
        AdapterConstants.movieType -> AdapterConstants.sectionMovie
        else -> AdapterConstants.sectionEmpty
    }

    private fun populateFaces(baseViewHolder: BaseViewHolder, position: Int) {
        val holder = baseViewHolder as? MovieListViewHolder
        val movieData = movieList?.getOrNull(position)
        holder?.bind(movieData,position)
    }
}