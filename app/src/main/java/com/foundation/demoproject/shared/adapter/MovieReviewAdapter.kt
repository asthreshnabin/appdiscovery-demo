package com.foundation.demoproject.shared.adapter

import android.content.Context
import android.view.ViewGroup
import com.foundation.demoproject.R
import com.foundation.demoproject.shared.base.BaseAdapter
import com.foundation.demoproject.shared.base.BaseViewHolder
import com.foundation.demoproject.shared.viewHolder.EmptyViewHolder
import com.foundation.demoproject.shared.viewHolder.MovieReviewViewHolder
import com.foundation.demoproject.utils.extensions.inflate
import com.usermodule.shared.model.response.MovieReview
import com.usermodule.utils.constants.AdapterConstants

/**
 * @Created by: asthreshNabin
 * @Date: 20/11/2022
 */
class MovieReviewAdapter(
    private var context: Context?,
    private val movieReviewList: MutableList<MovieReview?>?,
) : BaseAdapter<BaseViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        AdapterConstants.sectionReview -> MovieReviewViewHolder(
            parent.inflate(R.layout.review_item), context
        )
        else -> EmptyViewHolder(parent.inflate(R.layout.adapter_empty))
    }

    override fun onBindViewHolder(baseViewHolder: BaseViewHolder, position: Int) {
        when (getItemViewType(position)) {
            AdapterConstants.sectionReview -> populateFaces(baseViewHolder, position)
        }
    }

    override fun getItemCount() = movieReviewList?.count() ?: 0

    override fun getItemViewType(position: Int) =
        when (movieReviewList?.getOrNull(position)?.type) {
            AdapterConstants.reviewType -> AdapterConstants.sectionReview
            else -> AdapterConstants.sectionEmpty
        }

    private fun populateFaces(baseViewHolder: BaseViewHolder, position: Int) {
        val holder = baseViewHolder as? MovieReviewViewHolder
        val movieData = movieReviewList?.getOrNull(position)
        holder?.bind(movieData)
    }
}