package com.foundation.demoproject.shared.helper

import android.app.ProgressDialog
import android.content.Context


/**
 * Created by asthreshnabin on 111/19/2022.
 */
class ProgressDialogHelper(context: Context?) {
    private var progressDialog: ProgressDialog? = null

    init {
        progressDialog = ProgressDialog(context)
        progressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)
    }

    fun show(msg: String) {
        progressDialog?.setMessage(msg)
        progressDialog?.setCancelable(false)
        progressDialog?.show()
    }

    fun dismiss() {
        if (progressDialog == null) {
            return
        }
        progressDialog?.dismiss()
    }

}