package com.foundation.demoproject.feature.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.foundation.demoproject.databinding.ActivityLoginBinding
import com.foundation.demoproject.feature.main.MainActivity
import com.foundation.demoproject.feature.signup.SignupActivity
import com.foundation.demoproject.shared.base.BaseActivity
import com.foundation.demoproject.utils.extensions.OnViewClickListener
import com.foundation.demoproject.utils.extensions.onClickListener
import com.usermodule.shared.model.response.LoginResponse

class LoginActivity : BaseActivity<LoginView, LoginPresenter>(), LoginView {
    private var binding: ActivityLoginBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        setup()
    }

    override fun createPresenter() = LoginPresenter()

    private fun setup() {
        onClickListener(this, binding?.btnLogin, object : OnViewClickListener {
            override fun onClicked(context: Context?) {
                validateData(
                    binding?.edtUsername?.text?.toString()?.trim(),
                    binding?.edtPassword?.text?.toString()?.trim()
                )
            }
        })
        onClickListener(this, binding?.goToSignup, object : OnViewClickListener {
            override fun onClicked(context: Context?) {
                startActivity(Intent(this@LoginActivity,SignupActivity::class.java))
            }
        })
    }

    private fun validateData(username: String?, password: String?) {
        presenter?.doLogin(this@LoginActivity,username, password)
    }

    override fun onLoginSuccess(it: LoginResponse?) {
        finish()
        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
    }

    override fun onLoginFail(localizedMessage: String?) {
        showToast("Login Failed!!!")
    }
}