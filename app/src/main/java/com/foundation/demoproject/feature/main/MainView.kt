package com.foundation.demoproject.feature.main

import com.foundation.demoproject.shared.base.BaseView
import com.usermodule.shared.model.response.MovieResponse
import com.usermodule.shared.model.response.UserProfileModel


interface MainView: BaseView {
    fun populateView(movieList: MutableList<MovieResponse?>?)
    fun populateProfileData(profileData: UserProfileModel?)
}