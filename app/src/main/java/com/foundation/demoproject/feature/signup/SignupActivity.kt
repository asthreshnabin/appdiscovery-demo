package com.foundation.demoproject.feature.signup

import android.content.Context
import android.os.Bundle
import com.foundation.demoproject.databinding.ActivitySignupBinding
import com.foundation.demoproject.shared.base.BaseActivity
import com.foundation.demoproject.utils.PreferenceUtils
import com.foundation.demoproject.utils.extensions.OnViewClickListener
import com.foundation.demoproject.utils.extensions.onClickListener
import com.usermodule.shared.model.response.LoginResponse

class SignupActivity : BaseActivity<SignupView, SignupPresenter>(), SignupView {
    private var binding: ActivitySignupBinding? = null
    private var username: String? = null
    private var password: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignupBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        setup()
    }

    override fun createPresenter() = SignupPresenter()

    override fun onSignupFailed(localizedMessage: String?) {
        showToast("SignUp Failed!!")
    }

    override fun onSignupSuccess(response: LoginResponse?) {
        if (response != null) {
            PreferenceUtils.setUsername(this@SignupActivity,username)
            PreferenceUtils.setPassword(this@SignupActivity,password)
            showToast(response.message)
            finish()
        } else {
            showToast("SignUp Failed!!")
        }
    }

    private fun setup() {
        initListeners()
    }

    private fun initListeners() {
        onClickListener(this@SignupActivity, binding?.btnSignUp, object : OnViewClickListener {
            override fun onClicked(context: Context?) {
                validateData(
                    binding?.edtUsername?.text?.toString()?.trim(),
                    binding?.edtPassword?.text?.toString()?.trim(),
                    binding?.edtConfirmPassword?.text?.toString()?.trim()
                )
            }
        })
        onClickListener(this@SignupActivity, binding?.goToLogin, object : OnViewClickListener {
            override fun onClicked(context: Context?) {
                finish()
            }
        })
        onClickListener(this@SignupActivity, binding?.imvClose, object : OnViewClickListener {
            override fun onClicked(context: Context?) {
                finish()
            }
        })
    }

    private fun validateData(username: String?, password: String?, confirmPassword: String?) {
        this.username = username
        this.password = password
        presenter?.doSignUp(username, password, confirmPassword)
    }
}