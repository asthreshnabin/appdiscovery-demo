package com.foundation.demoproject.feature.signup

import com.foundation.demoproject.shared.base.BaseInteractor
import com.usermodule.shared.repository.SignUpRepository

/**
 * @Created by: asthreshNabin
 * @Date: 06/01/2023
 */
class SignupInteractor:BaseInteractor() {
    private val signUpRepository:SignUpRepository = SignUpRepository()
    fun doSignUp(username:String?,password:String?)=signUpRepository?.doSignUp(username,password)

}