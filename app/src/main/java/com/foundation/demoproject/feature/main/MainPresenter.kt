package com.foundation.demoproject.feature.main

import com.foundation.demoproject.shared.base.BasePresenter
import com.usermodule.shared.model.response.MovieResponse
import io.reactivex.disposables.CompositeDisposable

class MainPresenter : BasePresenter<MainView>() {
    private val mainInteractor = MainInteractor()

    fun getNowPlayingMovie() {
        ifViewAttached { view ->
            mainInteractor.getNowPlayingMovie().subscribe({ movieList ->
                view.populateView((movieList as MutableList<MovieResponse?>))
            }, {
                view.populateView(mutableListOf())
            }).let { CompositeDisposable().add(it) }
        }
    }
    fun getUserProfile() {
        ifViewAttached { view ->
            mainInteractor.getUserProfile().subscribe({ profileData ->
                view.populateProfileData((profileData))
            }, {
                view.populateView(mutableListOf())
            }).let { CompositeDisposable().add(it) }
        }
    }
}