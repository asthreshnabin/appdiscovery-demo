package com.foundation.demoproject.feature.movieDetails

import com.foundation.demoproject.shared.base.BaseInteractor
import com.usermodule.shared.repository.MovieDetailsRepository

class MovieDetailsInteractor:BaseInteractor() {
    private val movieDetailsRepository= MovieDetailsRepository()

    fun getMovieDetailsById(movieId:Long?)= movieDetailsRepository.getMovieDetailsById(movieId)
    fun getMovieReviewById(movieId:Long?)= movieDetailsRepository.getMovieReviewById(movieId)

}