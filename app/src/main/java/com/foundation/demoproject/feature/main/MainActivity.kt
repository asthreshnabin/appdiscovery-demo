package com.foundation.demoproject.feature.main

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.foundation.demoproject.databinding.ActivityMainBinding
import com.foundation.demoproject.feature.movieDetails.MovieDetailsActivity
import com.foundation.demoproject.shared.adapter.MovieListAdapter
import com.foundation.demoproject.shared.base.BaseActivity
import com.usermodule.shared.model.response.MovieResponse
import com.usermodule.shared.model.response.UserProfileModel
import com.usermodule.utils.constants.BundleConstants

class MainActivity : BaseActivity<MainView, MainPresenter>(), MainView {
    private var moviesList: MutableList<MovieResponse?>? = null
    private var binding: ActivityMainBinding? = null
    private var movieListAdapter: MovieListAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        setup()
    }

    override fun createPresenter() = MainPresenter()

    private fun setup() {
        initPresenter()
    }

    private fun initPresenter() {
        showLoading("Loading...")
        presenter?.getNowPlayingMovie()
        presenter?.getUserProfile()
    }

    override fun populateView(movieList: MutableList<MovieResponse?>?) {
        hideLoading()
        if (movieList.isNullOrEmpty() && movieList?.size == 0) {
            this.moviesList?.addAll(mutableListOf())

        } else {
            this.moviesList?.clear()
            this.moviesList?.addAll(movieList ?: mutableListOf())
            initRecyclerView(movieList)
        }
    }

    override fun populateProfileData(profileData: UserProfileModel?) {
        if (profileData != null) {
            Glide.with(this).load(profileData.image).apply(RequestOptions().circleCrop())
                .into(binding?.imvProfile!!)
        }
    }

    private fun initRecyclerView(movieList: MutableList<MovieResponse?>?) {
        binding?.rcvMovieList?.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding?.rcvMovieList?.isNestedScrollingEnabled = true
        binding?.rcvMovieList?.setHasFixedSize(true)
        movieListAdapter = MovieListAdapter(this, movieList) { position ->
            navigateToMovieDetails(position, movieList)
        }
        binding?.rcvMovieList?.adapter = movieListAdapter
        movieListAdapter?.notifyDataSetChanged()
    }

    private fun navigateToMovieDetails(position: Int, movieList: MutableList<MovieResponse?>?) {
        val movieData = movieList?.getOrNull(position)
        val intent = Intent(this, MovieDetailsActivity::class.java)
        intent.putExtra(BundleConstants.movieId, movieData?.Id)
        startActivity(intent)
    }
}