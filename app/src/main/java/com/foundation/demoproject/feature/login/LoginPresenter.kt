package com.foundation.demoproject.feature.login

import android.content.Context
import com.foundation.demoproject.R
import com.foundation.demoproject.shared.base.BasePresenter
import com.foundation.demoproject.utils.PreferenceUtils
import com.usermodule.utils.extensions.getSubscription
import io.reactivex.disposables.CompositeDisposable

/**
 * @Created by: asthreshNabin
 * @Date: 05/01/2023¬
 */
class LoginPresenter : BasePresenter<LoginView>() {
    private var loginInteractor: LoginInteractor? = LoginInteractor()
    fun doLogin(context: Context?, username: String?, password: String?) {
        ifViewAttached { view ->
            if (!isUserNameValid(username)) {
                view.showToast(R.string.txtEnterUserName)
            } else if (!isPasswordValid(password)) {
                view.showToast(R.string.txtEnterPassword)
            } else {
                if (username == PreferenceUtils.getUsername(context) && password == PreferenceUtils.getPassword(context)) {
                    view.showLoading("Signing in progress...")
                    loginInteractor?.doLogin(username, password).getSubscription()?.subscribe({
                        view.hideLoading()
                        view.onLoginSuccess(it)
                    }, {
                        view.hideLoading()
                        view.onLoginFail(it.localizedMessage)
                    })?.let { CompositeDisposable().add(it) }
                }else{
                    view.showToast(R.string.txtCredNotMatch)
                }
            }
        }
    }

    fun isUserNameValid(username: String?): Boolean = username?.isNotBlank() ?: false
    fun isPasswordValid(password: String?): Boolean = password?.isNotBlank() ?: false
}