package com.foundation.demoproject.feature.movieDetails

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.View
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.foundation.demoproject.BuildConfig
import com.foundation.demoproject.databinding.ActivityMovieDetailsBinding
import com.foundation.demoproject.shared.adapter.MovieReviewAdapter
import com.foundation.demoproject.shared.base.BaseActivity
import com.foundation.demoproject.utils.extensions.OnViewClickListener
import com.foundation.demoproject.utils.extensions.load
import com.foundation.demoproject.utils.extensions.onClickListener
import com.usermodule.BuildConfig.baseImageUrl
import com.usermodule.shared.model.response.MovieResponse
import com.usermodule.shared.model.response.MovieReview
import com.usermodule.utils.constants.BundleConstants
import java.text.DecimalFormat

class MovieDetailsActivity : BaseActivity<MovieDetailsView, MovieDetailsPresenter>(),
    MovieDetailsView {
    private var movieId: Long? = null
    private var binding: ActivityMovieDetailsBinding? = null
    private var movieReviewList: MutableList<MovieReview?>? = null
    private var movieReviewAdapter: MovieReviewAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieDetailsBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        setTransparentStatusBar()
        setup()
    }

    override fun createPresenter() = MovieDetailsPresenter()

    override fun populateView(movieDetail: MovieResponse?) {
        initView(movieDetail)
        hideLoading()

    }
    fun setTransparentStatusBar() {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
        window.statusBarColor = Color.TRANSPARENT
    }

    fun setWindowFlag(bits: Int, on: Boolean) {
        val win = window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }
    override fun populateReviewView(movieReview: MutableList<MovieReview?>) {
if (movieReview.isNotEmpty())
    initRecyclerView(movieReview)
    }

    private fun setup() {
        getExtras()
        initPresenter()
        initListeners()
    }
    private fun initRecyclerView(movieList: MutableList<MovieReview?>?) {
        binding?.rcvMovieReview?.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding?.rcvMovieReview?.isNestedScrollingEnabled = true
        binding?.rcvMovieReview?.setHasFixedSize(true)
        movieReviewAdapter = MovieReviewAdapter(this, movieList)
        binding?.rcvMovieReview?.adapter = movieReviewAdapter
        movieReviewAdapter?.notifyDataSetChanged()
    }
    private fun getExtras() {
        this.movieId = intent.getLongExtra(BundleConstants.movieId, 0)
    }

    private fun initPresenter() {
        if (this.movieId != null) {
            showLoading("Getting Movie Details...")
            presenter?.getMovieDetailsById(this.movieId)
        }
    }

    private fun initView(movieDetail: MovieResponse?) {
        binding?.imgMovieImage?.load("${BuildConfig.baseImageUrl}${movieDetail?.posterPath}")
        binding?.txvMovieName?.text = movieDetail?.title
        binding?.txvOverView?.text = movieDetail?.overview
        binding?.txvRunningTime?.text = String.format("Running Time: ${movieDetail?.runningTime}min")
        binding?.txvReleaseDate?.text = String.format("Release Date: ${movieDetail?.releaseDate}")
        val rating= DecimalFormat("#.#").format(movieDetail?.averageVote?.toDouble())
        val s = SpannableString(
            """
              ${rating}/10 
              """.trimIndent()
        )
        s.setSpan(RelativeSizeSpan(1.7f), 0, 3, 0)
        binding?.txvRating?.text = s
    }

    private fun initListeners() {
        onClickListener(this, binding?.toolbar?.actionBack, object : OnViewClickListener {
            override fun onClicked(context: Context?) {
                onBackPressed()
            }
        })
    }
}