package com.foundation.demoproject.feature.login

import com.foundation.demoproject.shared.base.BaseInteractor
import com.usermodule.shared.repository.LoginRepository

/**
 * @Created by: asthreshNabin
 * @Date: 05/01/2023
 */
class LoginInteractor: BaseInteractor() {
    private val loginRepository:LoginRepository? = LoginRepository()
    fun doLogin(username:String?,password:String?)=loginRepository?.doLogin(username,password)
}