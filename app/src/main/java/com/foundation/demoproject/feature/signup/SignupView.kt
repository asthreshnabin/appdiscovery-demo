package com.foundation.demoproject.feature.signup

import com.foundation.demoproject.shared.base.BaseView
import com.usermodule.shared.model.response.LoginResponse

/**
 * @Created by: asthreshNabin
 * @Date: 06/01/2023
 */
interface SignupView:BaseView {
    fun onSignupFailed(localizedMessage: String?)
    fun onSignupSuccess(response: LoginResponse?)
}