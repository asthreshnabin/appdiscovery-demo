package com.foundation.demoproject.feature.movieDetails

import com.foundation.demoproject.shared.base.BaseView
import com.usermodule.shared.model.response.MovieResponse
import com.usermodule.shared.model.response.MovieReview

interface MovieDetailsView:BaseView {
    fun populateView(movieDetail: MovieResponse?)
    fun populateReviewView(movieReview: MutableList<MovieReview?>)
}