package com.foundation.demoproject.feature.login

import com.foundation.demoproject.shared.base.BaseView
import com.usermodule.shared.model.response.LoginResponse

/**
 * @Created by: asthreshNabin
 * @Date: 05/01/2023
 */
interface LoginView:BaseView {
    fun onLoginSuccess(it: LoginResponse?)
    fun onLoginFail(localizedMessage: String?)
}