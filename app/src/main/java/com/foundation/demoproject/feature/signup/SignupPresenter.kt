package com.foundation.demoproject.feature.signup

import com.foundation.demoproject.R
import com.foundation.demoproject.shared.base.BasePresenter
import com.usermodule.utils.extensions.getSubscription
import io.reactivex.disposables.CompositeDisposable

/**
 * @Created by: asthreshNabin
 * @Date: 06/01/2023
 */
class SignupPresenter : BasePresenter<SignupView>() {
    private val signupInteractor: SignupInteractor = SignupInteractor()
    fun doSignUp(username: String?, password: String?, confirmPassword: String?) {
        ifViewAttached { view ->
            if (!isUserNameValid(username)) {
                view.showToast(R.string.txtEnterUserName)
            } else if (!isPasswordValid(password)) {
                view.showToast(R.string.txtEnterPassword)
            } else if (!isPasswordValid(confirmPassword)) {
                view.showToast(R.string.txtEnterConfirmPassword)
            } else if (!isPasswordMatche(password, confirmPassword)) {
                view.showToast(R.string.txtPasswordNotMatched)
            } else {
                view.showLoading("Signup in progress...")
                signupInteractor.doSignUp(username, password).getSubscription()?.subscribe({
                    view.hideLoading()
                    view.onSignupSuccess(it)
                }, {
                    view.hideLoading()
                    view.onSignupFailed(it.localizedMessage)
                })?.let { CompositeDisposable().add(it) }
            }
        }
    }

    fun isUserNameValid(username: String?): Boolean = username?.isNotBlank() ?: false
    fun isPasswordValid(password: String?): Boolean = password?.isNotBlank() ?: false
    fun isPasswordMatche(password: String?, confirmPassword: String?): Boolean =
        password?.equals(confirmPassword) ?: false
}