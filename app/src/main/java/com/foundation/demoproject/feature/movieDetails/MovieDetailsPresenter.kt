package com.foundation.demoproject.feature.movieDetails

import com.foundation.demoproject.shared.base.BasePresenter
import com.usermodule.shared.model.response.MovieResponse
import com.usermodule.shared.model.response.MovieReview
import io.reactivex.disposables.CompositeDisposable


class MovieDetailsPresenter:BasePresenter<MovieDetailsView>() {
    private var movieDetailsInteractor = MovieDetailsInteractor()
    fun getMovieDetailsById(movieId:Long?) {
        ifViewAttached { view ->
            movieDetailsInteractor.getMovieDetailsById(movieId).subscribe({ movieDetail ->
                view.populateView((movieDetail))
                getMovieReviewById(movieId)
            }, {
                view.populateView(MovieResponse())
            }).let { CompositeDisposable().add(it) }
        }
    }
    fun getMovieReviewById(movieId:Long?) {
        ifViewAttached { view ->
            movieDetailsInteractor.getMovieReviewById(movieId).subscribe({ movieReview ->
                view.populateReviewView((movieReview as MutableList<MovieReview?>))
            }, {
                view.populateView(MovieResponse())
            }).let { CompositeDisposable().add(it) }
        }
    }
}