package com.foundation.demoproject.feature.main

import com.foundation.demoproject.shared.base.BaseInteractor
import com.usermodule.shared.repository.MainRepository


class MainInteractor : BaseInteractor() {
    private val mainRepository = MainRepository()

    fun getNowPlayingMovie() = mainRepository.getNowPlayingMovie()
    fun getUserProfile() = mainRepository.getUserProfile()

}