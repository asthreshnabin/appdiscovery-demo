package com.foundation.demoproject.utils

import android.content.Context
import com.foundation.demoproject.utils.constants.PreferenceConstants

object PreferenceUtils {
    private fun getPreference(context: Context?) =
        context?.getSharedPreferences(PreferenceConstants.preferenceName, Context.MODE_PRIVATE)

    fun clearAll(context: Context?) {
        getPreference(context)?.edit()?.clear()?.apply()
    }


    fun getUsername(context: Context?) =
        getPreference(context)?.getString(PreferenceConstants.username, null)

    fun setUsername(context: Context?, username: String?) {
        val editor = getPreference(context)?.edit()
        editor?.putString(PreferenceConstants.username, username)
        editor?.apply()
    }
    fun getPassword(context: Context?) =
        getPreference(context)?.getString(PreferenceConstants.password, null)

    fun setPassword(context: Context?, password: String?) {
        val editor = getPreference(context)?.edit()
        editor?.putString(PreferenceConstants.password, password)
        editor?.apply()
    }


}