package com.foundation.demoproject.utils.extensions

import android.widget.ImageView
import com.foundation.demoproject.R
import com.squareup.picasso.Picasso

/**
 * @Created by: asthreshNabin
 * @Date: 19/11/2022
 */
fun ImageView?.load(url: String?, placeholder: Int = R.drawable.bg_cloud) {
    if (!url.isNullOrBlank()) {
        Picasso.get()
            .load(url)
            .error(placeholder)
            .placeholder(placeholder)
            .into(this)
    } else {
        this?.setImageResource(placeholder)
    }
}