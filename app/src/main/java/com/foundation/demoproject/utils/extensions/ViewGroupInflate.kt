package com.foundation.demoproject.utils.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * @Created by: asthreshNabin
 * @Date: 19/11/2022
 */
fun ViewGroup.inflate(layout: Int): View =
    LayoutInflater.from(this.context).inflate(layout, this, false)