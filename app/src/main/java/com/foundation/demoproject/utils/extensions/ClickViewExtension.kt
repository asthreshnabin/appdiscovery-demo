package com.foundation.demoproject.utils.extensions

import android.content.Context
import android.view.View
import com.jakewharton.rxbinding.view.RxView
import java.util.concurrent.TimeUnit

/**
 * @Created by: asthreshNabin
 * @Date: 19/11/2022
 */
fun onClickListener(context: Context?, view: View?, listener: OnViewClickListener?) {
    view?.let {
        RxView.clicks(it).throttleFirst(2000, TimeUnit.MILLISECONDS)
            .subscribe { listener?.onClicked(context) }
    }
}

interface OnViewClickListener {
    fun onClicked(context: Context?)
}