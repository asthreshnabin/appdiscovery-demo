package com.usermodule.shared.model.response

import com.usermodule.utils.constants.AdapterConstants
import com.google.gson.annotations.SerializedName

/**
 * @Created by: asthreshNabin
 * @Date: 01/05/2023.
 */
data class MovieResponse(
    @SerializedName("id")
    var Id:Long? = null,
    @SerializedName("title")
    var title:String? = null,
    @SerializedName("overview")
    var overview:String? = null,
    @SerializedName("poster_path")
    var posterPath:String? = null,
    @SerializedName("release_date")
    var releaseDate:String? = null,
    @SerializedName("runtime")
    var runningTime:Long? = null,
    @SerializedName("vote_average")
    var averageVote:Float? = null,
    @SerializedName("type")
    val type: String? = AdapterConstants.movieType,

    )
