package com.usermodule.shared.model.response.error

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Error (
    @SerializedName("title")
    @Expose
     val title: String? = null,
    @SerializedName("detail")
    @Expose
     val detail: String? = null

)