package com.usermodule.shared.model.response

import com.google.gson.annotations.SerializedName

/**
 * @Created by: asthreshNabin
 * @Date: 05/01/2023
 */
data class LoginResponse(
    @SerializedName("message")
    var message:String? = null,
)
