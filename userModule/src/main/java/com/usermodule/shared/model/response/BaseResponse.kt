package com.usermodule.shared.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by asthreshnabin on 01/05/2023.
 */
data class BaseResponse<T>(
    @SerializedName("results")
    var data: T? = null,

)