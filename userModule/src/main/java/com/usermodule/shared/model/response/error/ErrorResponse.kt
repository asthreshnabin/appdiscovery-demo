package com.usermodule.shared.model.response.error

import com.usermodule.shared.model.response.error.Error
import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("error")
    var errors: Error? = null,

    @SerializedName("errors")
    var errorList: List<Error?>? = null
)