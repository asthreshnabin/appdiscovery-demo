package com.usermodule.shared.model.response

import com.google.gson.annotations.SerializedName

/**
 * @Created by: asthreshNabin
 * @Date: 05/01/2023
 */
data class UserProfileModel(
    @SerializedName("id")
    var id:Int? = null,
    @SerializedName("firstName")
    var firstName:String? = null,
    @SerializedName("lastName")
    var lastName:String? = null,
    @SerializedName("image")
    var image:String? = null,
    @SerializedName("address")
    var address:String? = null,
    @SerializedName("phoneNumber")
    var phoneNumber:String? = null,
)
