package com.usermodule.shared.model.request

import com.google.gson.annotations.SerializedName

/**
 * @Created by: asthreshNabin
 * @Date: 05/01/2023
 */
data class UserDataModel(
    @SerializedName("userName")
    var userName:String? = null,
    @SerializedName("password")
    var password:String? = null,

)
