package com.usermodule.shared.model.response

import com.google.gson.annotations.SerializedName
import com.usermodule.utils.constants.AdapterConstants

/**
 * @Created by: asthreshNabin
 * @Date: 20/11/2022
 */
data class MovieReview(
    @SerializedName("author")
    var author:String? = null,
    @SerializedName("content")
    var content:String? = null,
    @SerializedName("created_at")
    var createdAt:String? = null,
    @SerializedName("id")
    var id:String? = null,
    @SerializedName("updated_at")
    var updatedAt:String? = null,
    @SerializedName("url")
    var url:String? = null,
    @SerializedName("author_details")
    var authorDetails:AuthorDetails? = null,

    @SerializedName("type")
    val type: String? = AdapterConstants.reviewType,
)

data class AuthorDetails(
    @SerializedName("name")
    var name:String? = null,
    @SerializedName("username")
    var username:String? = null,
    @SerializedName("avatar_path")
    var avatar_path:String? = null,
    @SerializedName("rating")
    var rating:Float? = null,
)

