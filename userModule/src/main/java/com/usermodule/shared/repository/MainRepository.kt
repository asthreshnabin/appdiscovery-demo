package com.usermodule.shared.repository

import com.usermodule.shared.model.response.MovieResponse
import com.usermodule.utils.extensions.getSubscription
import com.usermodule.BuildConfig
import com.usermodule.shared.model.response.UserProfileModel
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

class MainRepository: BaseRepository() {
    fun getNowPlayingMovie(): Single<List<MovieResponse?>> =
        Single.create { e ->
            apiService.getNowPlayingMovie(BuildConfig.apiKey)
                .getSubscription()
                ?.subscribe({
                    if (it.body()?.data?.size!!>0) {
                        e.onSuccess(it.body()?.data ?: emptyList())
                    } else {
                        e.onError(getError(it.errorBody()?.string()))
                    }
                }, {
                    e.onError(it)
                })?.let { CompositeDisposable().add(it) }
        }
    fun getUserProfile(): Single<UserProfileModel?> =
        Single.create { e ->
            loginApiService.getUserProfile()
                .getSubscription()
                ?.subscribe({
                    if (it.body() != null) {
                        e.onSuccess(it.body() ?: UserProfileModel())
                    } else {
                        e.onError(getError(it.errorBody()?.string()))
                    }
                }, {
                    e.onError(it)
                })?.let { CompositeDisposable().add(it) }
        }
}