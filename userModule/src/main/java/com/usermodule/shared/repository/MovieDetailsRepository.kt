package com.usermodule.shared.repository

import com.usermodule.BuildConfig
import com.usermodule.shared.model.response.MovieResponse
import com.usermodule.shared.model.response.MovieReview
import com.usermodule.utils.extensions.getSubscription
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

class MovieDetailsRepository : BaseRepository() {
    fun getMovieDetailsById(movieId: Long?): Single<MovieResponse?> =
        Single.create { e ->
            apiService.getMovieDetailsById(movieId, BuildConfig.apiKey)
                .getSubscription()
                ?.subscribe({
                    if (it.body() != null) {
                        e.onSuccess(it.body() ?: MovieResponse())
                    } else {
                        e.onError(getError(it.errorBody()?.string()))
                    }
                }, {
                    e.onError(it)
                })?.let { CompositeDisposable().add(it) }
        }

    fun getMovieReviewById(movieId: Long?): Single<List<MovieReview?>> =
        Single.create { e ->
            apiService.getMovieReviewById(movieId,BuildConfig.apiKey)
                .getSubscription()
                ?.subscribe({
                    if (it.body() != null) {
                        e.onSuccess(it.body()?.data ?: mutableListOf())
                    } else {
                        e.onError(getError(it.errorBody()?.string()))
                    }
                }, {
                    e.onError(it)
                })?.let { CompositeDisposable().add(it) }
        }
}