package com.usermodule.shared.repository

import com.usermodule.network.RetrofitHelper
import com.google.gson.Gson
import com.usermodule.shared.model.response.error.ErrorResponse
import com.usermodule.utils.GlobalUtils
import com.usermodule.utils.constants.StringConstants
import okhttp3.RequestBody

/**
 * Created by asthreshnabin on 01/05/2023.
 */
abstract class BaseRepository {
    val apiService = RetrofitHelper.getApiService()
    val loginApiService = RetrofitHelper.getLoginService()
    fun getGlobalUtils(any: Any): RequestBody =
        GlobalUtils.buildGson(any)

    private fun getDefaultError() = Throwable(StringConstants.errorMessage)

    fun getError(error: String?):Throwable {
        return try {
            val gson = Gson()
            val root = gson.fromJson(error, ErrorResponse::class.java)
            val errorMsg = root?.errors
            val errorList = root?.errorList
            if (errorMsg != null) {
                Throwable(errorMsg.detail)
            } else if (!errorList.isNullOrEmpty())
                Throwable(errorList[0]?.detail)
            else
                getDefaultError()
        } catch (e: Exception) {
            getDefaultError()
        }
    }

    fun getDefaultThrowableError(throwable: Throwable?): Throwable {
        return if (throwable?.message == null)
            getDefaultError()
        else throwable
    }


}