package com.usermodule.shared.repository

import com.usermodule.shared.model.response.LoginResponse
import com.usermodule.utils.extensions.getSubscription
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

/**
 * @Created by: asthreshNabin
 * @Date: 06/01/2023
 */
class SignUpRepository:BaseRepository() {
    fun doSignUp(username: String?, password: String?): Single<LoginResponse?> =
        Single.create { e ->
            loginApiService.doSignUp(username, password)
                .getSubscription()
                ?.subscribe({
                    if (it.body() != null) {
                        e.onSuccess(it.body() ?: LoginResponse())
                    } else {
                        e.onError(getError(it.errorBody()?.string()))
                    }
                }, {
                    e.onError(it)
                })?.let { CompositeDisposable().add(it) }
        }
}