package com.usermodule.network

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.usermodule.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 */
object RetrofitHelper {
    private lateinit var apiService: ApiService
    private lateinit var loginApiService: ApiService
    private val gson = GsonBuilder().setLenient().create()

    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private val okHttpClient = OkHttpClient.Builder()
        .readTimeout(1, TimeUnit.MINUTES)
        .writeTimeout(1, TimeUnit.MINUTES)
        .addInterceptor(loggingInterceptor)
        .build()

    private val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(BuildConfig.baseUrl)
        .client(okHttpClient)
        .build()
    private val loginRetrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(BuildConfig.baseAuthUrl)
        .client(okHttpClient)
        .build()

    fun getApiService(): ApiService {
        if (!RetrofitHelper::apiService.isInitialized)
            apiService = retrofit.create(ApiService::class.java)
        return apiService
    }
    fun getLoginService(): ApiService {
        if (!RetrofitHelper::loginApiService.isInitialized)
            loginApiService = loginRetrofit.create(ApiService::class.java)
        return loginApiService
    }

}