package com.usermodule.network

import com.usermodule.shared.model.response.*
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @POST("login")
    fun doLogin(
        @Query("username") username: String?, @Query("password") password: String?
    ): Single<Response<LoginResponse?>>
    @POST("signup")
    fun doSignUp(
        @Query("username") username: String?, @Query("password") password: String?
    ): Single<Response<LoginResponse?>>

    @GET("userDetails")
    fun getUserProfile(): Single<Response<UserProfileModel?>>

    @GET("now_playing")
    fun getNowPlayingMovie(
        @Query("api_key") apiKey: String?
    ): Single<Response<BaseResponse<List<MovieResponse?>>>>

    @GET("{movie_id}")
    fun getMovieDetailsById(
        @Path("movie_id") movie_id: Long?,
        @Query("api_key") apiKey: String?,
    ): Single<Response<MovieResponse?>>

    @GET("{movie_id}/reviews")
    fun getMovieReviewById(
        @Path("movie_id") movie_id: Long?,
        @Query("api_key") apiKey: String?,
    ): Single<Response<BaseResponse<List<MovieReview?>>>>
}