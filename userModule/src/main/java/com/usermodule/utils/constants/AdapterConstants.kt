package com.usermodule.utils.constants

object AdapterConstants {

    const val sectionMovie = 100
    const val movieType = "movie"
    const val sectionReview = 101
    const val reviewType = "review"

    const val sectionEmpty = 3335
}
