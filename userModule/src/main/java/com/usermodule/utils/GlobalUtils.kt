package com.usermodule.utils


import com.google.gson.GsonBuilder
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody


object GlobalUtils {

    fun buildGson(any: Any): RequestBody {
        val builder = GsonBuilder()
        val gson = builder.create()
        val json = gson.toJson(any)
        return RequestBody.create("application/json".toMediaTypeOrNull(), json)
    }

}